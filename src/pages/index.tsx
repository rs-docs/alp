import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import styles from "./index.module.css";
import { Footer } from "../components/Footer";
import Main from "../components/Main";
import { Header } from "../components/Header";

export default function Home(): JSX.Element {
  const { siteConfig } = useDocusaurusContext();
  return (
    <Layout title={`${siteConfig.title}`}>
      <div className={clsx("col col--12")}>
        <div
          className="container"
          style={{ marginTop: "70px", marginBottom: "70px" }}
        >
          <div className="row">
            <Header />
          </div>
        </div>
        <div
          style={{ justifyContent: "center" }}
          className={clsx("hero hero--primary", styles.heroBanner)}
        >
          <Main />
        </div>
        <div
          className="container"
          style={{ marginTop: "70px", marginBottom: "70px" }}
        >
          <Footer />
        </div>
      </div>
    </Layout>
  );
}
