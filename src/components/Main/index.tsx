import React from 'react';
export default function Main(): JSX.Element {
  return (
    <main>
        <div>
        <h1 className="text--center padding-horiz--md">
        Launch ALP Project Like A Pro
        </h1>
          <p className="text--center padding-horiz--md">
          Documentation is always with you to start the ALP project and achieve your goals. Everything you need to promote ALP is here.
          </p>
        </div>
    </main>
  );
}