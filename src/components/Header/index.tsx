import React from 'react';
import Logo from './knowledge.svg';
export function Header(): JSX.Element {
  return (
    <header className="header">
      <div className="logo">
      <Logo width={400} height={400} />
        </div>
      <div>
      <h1 className="text--center padding-horiz--md">
        ALP Help Center
      </h1>
      </div>
      <div>
      <p className="text--center padding-horiz--md">
        Quickly access the information you are looking for about the ALP.
      </p>
      </div>

    </header>
  );
}
