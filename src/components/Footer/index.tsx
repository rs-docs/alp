import React from "react";
import Tabs from "@theme/Tabs";
import TabItem from "@theme/TabItem";
import Link from "@docusaurus/Link";

export function Footer() {
  return (
    <div>
      <div style={{ marginBottom: "50px" }}>
        <h1 className="text--center padding-horiz--md">
          Frequently Asked Questions
        </h1>
        <p className="text--center padding-horiz--md">
          Quickly access the frequently asked questions of our customers
        </p>
      </div>
      <div>
        <Tabs>
          <TabItem
            value="contactus"
            label="How can we contact your technical support team ?"
            default
          >
            <p>
              We recommend to use our Help Desk application to let us know about
              problems you encounter with ALP. In this way, you can follow all
              the processes of the problem you have reported instantly and
              provide feedback.
            </p>

            <p>
              To access our Help Desk application, you can click on the{" "}
              <Link to="https://remmsoft.atlassian.net/servicedesk/customer/portal/1">
                link
              </Link>
              .
            </p>
          </TabItem>
          <TabItem value="upgrade" label="How do you do software updates ?">
            Before your customization requests or any updates to fix errors,
            your data is backed up and system users are informed before starting
            the process.
          </TabItem>
        </Tabs>
      </div>
    </div>
  );
}
